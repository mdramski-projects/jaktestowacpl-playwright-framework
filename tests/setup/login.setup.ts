import { STORAGE_STATE } from '@_pw-config';
import { expect, test as setup } from '@_src/fixtures/merge.fixture';
import { testUser1 } from '@_src/test-data/user.data';

setup('login and save session @GAD-R02-01', async ({ loginPage, page }) => {
    // Arrange
    const expectedWelcomeTitle = 'Welcome';

    const welcomePage = await loginPage.login(testUser1);
    const title = await welcomePage.getTitle();

    // Assert
    // eslint-disable-next-line playwright/no-standalone-expect
    expect(title).toContain(expectedWelcomeTitle);
    await page.context().storageState({ path: STORAGE_STATE });
});
