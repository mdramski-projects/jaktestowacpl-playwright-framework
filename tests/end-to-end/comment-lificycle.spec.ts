import { prepareRandomComment } from '@_src/factories/comment.factory';
import { expect, test } from '@_src/fixtures/merge.fixture';

test.describe('Verify comments', () => {
    test('operate on comments @GAD-R05-01 @GAD-R05-02 @GAD-R05-03 @logged', async ({
        createRandomArticle,
    }) => {
        // Arrange
        const expectedAddCommentCreatedPopup = 'Comment was created';
        const expectedAddCommentHeader = 'Add New Comment';

        let articlePage = createRandomArticle.articlePage;

        const newCommentData = prepareRandomComment();
        const editCommentData = prepareRandomComment();

        await test.step('Create new comment', async () => {
            // Act
            const commentView = await articlePage.clickAddCommentButton();
            await expect
                .soft(commentView.addNewHeader)
                .toHaveText(expectedAddCommentHeader);
            articlePage = await commentView.createComment(newCommentData);

            // Assert
            await expect.soft(articlePage.alertPopup).toBeVisible();
            await expect
                .soft(articlePage.alertPopup)
                .toHaveText(expectedAddCommentCreatedPopup);
        });

        let commentPage = await test.step('Verify comment', async () => {
            // Act
            const articleComment = articlePage.getArticleComment(
                newCommentData.body,
            );
            await expect(articleComment.body).toHaveText(newCommentData.body);
            const commentPage =
                await articlePage.clickCommentLink(articleComment);

            // Assert
            await commentPage.waitForPageToLoadUrl();
            await expect(commentPage.commentBody).toHaveText(
                newCommentData.body,
            );

            return commentPage;
        });

        await test.step('Update comment', async () => {
            // Arrange
            const expectedCommentEditedPopup = 'Comment was updated';

            // Act
            const editCommentView = await commentPage.clickEditButton();
            commentPage = await editCommentView.updateComment(editCommentData);

            // Assert
            await expect
                .soft(commentPage.alertPopup)
                .toHaveText(expectedCommentEditedPopup);
            await expect(commentPage.commentBody).toHaveText(
                editCommentData.body,
            );
        });

        await test.step('Verify updated comment', async () => {
            // Act
            const articlePage = await commentPage.clickReturnLink();
            const updatedArticleComment = articlePage.getArticleComment(
                editCommentData.body,
            );

            // Assert
            await expect(updatedArticleComment.body).toHaveText(
                editCommentData.body,
            );
        });
    });

    test('user can add more than one comment @GAD-R05-03 @logged', async ({
        createRandomArticle,
    }) => {
        let articlePage = createRandomArticle.articlePage;
        await test.step('Create first comment', async () => {
            // Arrange
            const expectedAddCommentCreatedPopup = 'Comment was created';

            const newCommentData = prepareRandomComment();

            // Act
            const commentView = await articlePage.clickAddCommentButton();
            articlePage = await commentView.createComment(newCommentData);

            // Assert
            await expect
                .soft(articlePage.alertPopup)
                .toHaveText(expectedAddCommentCreatedPopup);
        });

        await test.step('Create and verify another comment', async () => {
            const anotherCommentData =
                await test.step('Create another comment', async () => {
                    // Arrange
                    const anotherCommentData = prepareRandomComment();

                    // Act
                    const commentView =
                        await articlePage.clickAddCommentButton();
                    articlePage =
                        await commentView.createComment(anotherCommentData);
                    return anotherCommentData;
                });

            await test.step('Verify another comment', async () => {
                // Assert
                const articleComment = articlePage.getArticleComment(
                    anotherCommentData.body,
                );
                await expect(articleComment.body).toHaveText(
                    anotherCommentData.body,
                );
                const commentPage =
                    await articlePage.clickCommentLink(articleComment);

                await commentPage.waitForPageToLoadUrl();
                await expect(commentPage.commentBody).toHaveText(
                    anotherCommentData.body,
                );
            });
        });
    });
});
