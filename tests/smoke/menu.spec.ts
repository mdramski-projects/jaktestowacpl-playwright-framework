import { expect, test } from '@_src/fixtures/merge.fixture';

test.describe('Verify menu main buttons', () => {
    test('comments button navigates to comments page @GAD-R01-03 @chromium-non-logged', async ({
        articlesPage,
    }) => {
        // Arrange
        const expectedCommentsPageTitle = 'Comments';

        // Act
        const commentsPage = await articlesPage.mainMenu.clicksCommentsButton();
        const title = await commentsPage.getTitle();

        // Assert
        expect(title).toContain(expectedCommentsPageTitle);
    });

    test('articles button navigates to articles page @GAD-R01-03 @chromium-non-logged', async ({
        commentsPage,
    }) => {
        // Arrange
        const expectedArticlesPageTitle = 'Articles';

        // Act
        const articlesPage = await commentsPage.mainMenu.clicksArticlesButton();
        const title = await articlesPage.getTitle();

        // Assert
        expect(title).toContain(expectedArticlesPageTitle);
    });

    test('home page button navigates to main page @GAD-R01-03 @chromium-non-logged', async ({
        articlesPage,
    }) => {
        // Arrange
        const expectedHomePageTitle = 'GAD';

        // Act
        const homePage = await articlesPage.mainMenu.clicksHomePageLink();
        const title = await homePage.getTitle();

        // Assert
        expect(title).toContain(expectedHomePageTitle);
    });
});
