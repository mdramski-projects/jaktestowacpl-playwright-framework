import { prepareRandomNewArticle } from '@_src/factories/article.factory';
import { expect, test } from '@_src/fixtures/merge.fixture';
import { waitForResponse } from '@_src/utils/wait.util';

test.describe('Verify articles', () => {
    test('create new article @GAD-R04-01 @logged', async ({
        addArticleView,
    }) => {
        // Arrange
        const articleData = prepareRandomNewArticle();

        // Act
        const articlePage = await addArticleView.createArticle(articleData);

        // Assert
        await expect
            .soft(articlePage.articleTitle)
            .toHaveText(articleData.title);
        await expect
            .soft(articlePage.articleBody)
            .toHaveText(articleData.body, { useInnerText: true });
    });

    test('do not create new article with no title @GAD-R04-02 @logged', async ({
        addArticleView,
    }) => {
        // Arrange
        const expectedErrorText = 'Article was not created';
        const articleData = prepareRandomNewArticle();
        articleData.title = '';

        // Act
        await addArticleView.createArticle(articleData);

        // Assert
        await expect(addArticleView.alertPopup).toHaveText(expectedErrorText);
    });

    test('do not create new article with no title using api @GAD-R04-02 @logged', async ({
        addArticleView,
        page,
    }) => {
        // Arrange
        const expectedErrorText = 'Article was not created';
        const articleData = prepareRandomNewArticle();
        articleData.title = '';
        const expectedResponseCode = 422;

        const responsePromise = waitForResponse({ page, url: '/api/articles' });

        // Act
        await addArticleView.createArticle(articleData);
        const response = await responsePromise;

        // Assert
        await expect(addArticleView.alertPopup).toHaveText(expectedErrorText);
        expect(response.status()).toBe(expectedResponseCode);
    });

    test('do not create new article with no body @GAD-R04-02 @logged', async ({
        addArticleView,
    }) => {
        // Arrange
        const expectedErrorText = 'Article was not created';
        const articleData = prepareRandomNewArticle();
        articleData.body = '';

        // Act
        await addArticleView.createArticle(articleData);

        // Assert
        await expect(addArticleView.alertPopup).toHaveText(expectedErrorText);
    });

    test('user can access single article @GAD-R04-03 @logged', async ({
        page,
        addArticleView,
    }) => {
        // Arrange

        const articleData = prepareRandomNewArticle();
        const articlePage = await addArticleView.createArticle(articleData);

        // Act
        await page.getByText(articleData.title).click();

        // Assert
        await expect
            .soft(articlePage.articleTitle)
            .toHaveText(articleData.title);
        await expect
            .soft(articlePage.articleBody)
            .toHaveText(articleData.body, { useInnerText: true });
    });

    test.describe('Title length', () => {
        test('reject creating article with title exceeding 129 signs @GAD-R04-02 @logged', async ({
            addArticleView,
        }) => {
            // Arrange
            const expectedErrorText = 'Article was not created';
            const articleData = prepareRandomNewArticle(129);

            // Act
            await addArticleView.createArticle(articleData);

            // Assert
            await expect(addArticleView.alertPopup).toHaveText(
                expectedErrorText,
            );
        });

        test('reject creating article with title exceeding 129 signs using api @GAD-R04-02 @logged', async ({
            addArticleView,
            page,
        }) => {
            // Arrange
            const expectedErrorText = 'Article was not created';
            const articleData = prepareRandomNewArticle(129);
            const expectedResponseCode = 422;

            const responsePromise = waitForResponse({
                page,
                url: '/api/articles',
            });

            // Act
            await addArticleView.createArticle(articleData);
            const response = await responsePromise;

            // Assert
            expect(response.status()).toBe(expectedResponseCode);
            await expect(addArticleView.alertPopup).toHaveText(
                expectedErrorText,
            );
        });

        test('creating article with title 128 signs @GAD-R04-02 @logged', async ({
            addArticleView,
        }) => {
            // Arrange
            const articleData = prepareRandomNewArticle(128);

            // Act
            const articlePage = await addArticleView.createArticle(articleData);

            // Assert
            await expect
                .soft(articlePage.articleTitle)
                .toHaveText(articleData.title);
        });

        test('creating article with title 128 signs using api @GAD-R04-02 @logged', async ({
            addArticleView,
            page,
        }) => {
            // Arrange
            const articleData = prepareRandomNewArticle(128);
            const expectedResponseCode = 201;

            const responsePromise = waitForResponse({
                page,
                url: '/api/articles',
            });

            // Act
            const articlePage = await addArticleView.createArticle(articleData);
            const response = await responsePromise;

            // Assert
            expect(response.status()).toBe(expectedResponseCode);
            await expect
                .soft(articlePage.articleTitle)
                .toHaveText(articleData.title);
        });
    });

    test('should return created article from api @GAD-R04-04 @logged', async ({
        addArticleView,
        page,
    }) => {
        // Arrange
        const articleData = prepareRandomNewArticle();

        const responsePromise = waitForResponse({
            page,
            url: '/api/articles',
            method: 'GET',
        });

        // Act
        const articlePage = await addArticleView.createArticle(articleData);
        const response = await responsePromise;

        // Assert
        await expect
            .soft(articlePage.articleTitle)
            .toHaveText(articleData.title);
        expect(response.ok()).toBeTruthy();
    });
});
