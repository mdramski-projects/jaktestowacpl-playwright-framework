import { expect, test } from '@_src/fixtures/merge.fixture';

import { prepareRandomUserData } from '@_src/factories/user.factory';
import { RegisterUserModel } from '@_src/models/user.model';

test.describe('Verify register', () => {
    let registerUserData: RegisterUserModel;

    test.beforeEach(async ({}) => {
        registerUserData = prepareRandomUserData();
    });

    test('register with correct data and login @GAD-R03-01 @GAD-R03-02 @GAD-R03-03', async ({
        registerPage,
    }) => {
        // Arrange
        const expectAlertPopupText = 'User created';
        const expectedLoginTitle = 'Login';
        const expectedWelcomeTitle = 'Welcome';

        // Act
        const loginPage = await registerPage.register(registerUserData);

        // Assert
        await expect(registerPage.alertPopup).toHaveText(expectAlertPopupText);
        await loginPage.waitForPageToLoadUrl();
        const loginTitle = await loginPage.getTitle();
        expect(loginTitle).toContain(expectedLoginTitle);

        const welcomePage = await loginPage.login({
            userEmail: registerUserData.userEmail,
            userPassword: registerUserData.userPassword,
        });
        const welcomeTitle = await welcomePage.getTitle();

        // Assert
        expect(welcomeTitle).toContain(expectedWelcomeTitle);
    });

    test('not register with incorrect data - non valid email @GAD-R03-04', async ({
        registerPage,
    }) => {
        // Arrange
        const expectErrorText = 'Please provide a valid email address';

        registerUserData.userEmail = '@#$';

        // Act
        await registerPage.register(registerUserData);

        // Assert
        await expect(registerPage.emailErrorText).toHaveText(expectErrorText);
    });

    test('not register with incorrect data - email not provided @GAD-R03-04', async ({
        registerPage,
    }) => {
        // Arrange
        const expectErrorText = 'This field is required';

        // Act
        await registerPage.userFirstNameInput.fill(
            registerUserData.userFirstName,
        );
        await registerPage.userLastNameInput.fill(
            registerUserData.userLastName,
        );
        await registerPage.userPasswordInput.fill(
            registerUserData.userPassword,
        );
        await registerPage.registerButton.click();

        // Assert
        await expect(registerPage.emailErrorText).toHaveText(expectErrorText);
    });
});
