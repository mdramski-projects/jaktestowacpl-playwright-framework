import { AddCommentModel } from '@_src/models/comment.model';
import { CommentPage } from '@_src/pages/comment.page';
import { Page } from '@playwright/test';

export class EditCommentView {
    commentBodyInput = this.page.getByTestId('body-input');
    commentUpdateButton = this.page.getByTestId('update-button');

    alertPopup = this.page.getByTestId('alert-popup');

    constructor(private page: Page) {}

    async updateComment(commentData: AddCommentModel): Promise<CommentPage> {
        await this.commentBodyInput.fill(commentData.body);
        await this.commentUpdateButton.click();

        return new CommentPage(this.page);
    }
}
