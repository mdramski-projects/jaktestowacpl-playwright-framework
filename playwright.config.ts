import { BASE_URL } from '@_config/env.config';
import { defineConfig, devices } from '@playwright/test';
import * as path from 'path';

/**
 * See https:
// import { BASE_URL } from './src/global-setup';

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export const STORAGE_STATE = path.join(__dirname, 'tmp/session.json');
export const RESPONSE_TIMEOUT = 10_000;

export default defineConfig({
    testDir: './tests',
    globalSetup: 'config/global.setup.ts',
    timeout: 60_000,
    expect: { timeout: 10_000 },
    fullyParallel: true,
    retries: 0,
    workers: undefined,
    reporter: 'html',
    use: {
        baseURL: BASE_URL,
        actionTimeout: 0,
        trace: 'retain-on-failure',
        video: 'retain-on-failure',
        screenshot: 'only-on-failure',
    },
    // Projects can be found in the https://playwright.dev/docs/emulation
    // or ...node_modules\playwright-core\lib\server\deviceDescriptorsSource.json
    projects: [
        {
            name: 'chromium-non-logged',
            grepInvert: /@logged/,
            use: { ...devices['Desktop Chrome'] },
        },
        {
            name: 'setup',
            testMatch: '*.setup.ts',
        },
        {
            name: 'chromium-logged',
            grep: /@logged/,
            dependencies: ['setup'],
            use: {
                ...devices['Desktop Chrome'],
                storageState: STORAGE_STATE,
            },
        },
        // {
        //     name: 'iphone',
        //     use: { ...devices['iPhone 12 Mini'] },
        // },
        // {
        //     name: 'smoke',
        //     testDir: './tests/smoke',
        // },
    ],
});
