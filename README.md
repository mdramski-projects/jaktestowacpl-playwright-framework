# Tests for GAD application

### !!! NOTE !!!

This framework is part of a course from https://jaktestowac.pl

Course: Profesjonalny framework do testów z Playwright

## GAD Application

Repository: https://github.com/jaktestowac/gad-gui-api-demo

Follow instructions in app README

## Prepare

### Local recommended tools:

-   VS Code
-   Git
-   Node.js (version >16)

### Installation and setup

-   (optional) install VSC recommended plugins
-   install dependencies: `npm install`
-   setup Playwright with: `npx playwright install --with-deps chromium`
-   setup husky with: `npx husky install`
-   prepare local env file: `cp .env-template .env`,
-   copy application main URL as value of `BASE_URL` variable in `.env` file

## Use

Run all tests:

```
npx playwright test
```

Run all tests with given tag:

```
npx playwright test --grep @GAD-R01092
```

```
npx playwright test --grep "@GAD"
```

Run all tests WITHOUT given tag:

```
npx playwright test --grep-invert @GAD-R01092
```

SPECIAL WINDOWS CASES:

For CMD console:

```
npx playwright test --grep "@GAD-R01-01^|@GAD-R01-02"
```

For POWERSHELL console:

```
npx playwright test --grep --% "@GAD-R01-01^|@GAD-R01-02"
```

For more usage cases look in `package.json` scripts section.
